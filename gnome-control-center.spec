%define gnome_online_accounts_version 3.25.3
%define glib2_version 2.70.0
%define gnome_desktop_version 42.0
%define gsd_version 41.0
%define gsettings_desktop_schemas_version 42.0
%define upower_version 0.99.8
%define gtk4_version 4.9.3
%define gnome_bluetooth_version 42.0
%define libadwaita_version 1.2.0
%define nm_version 1.24

Name:           gnome-control-center
Version:        44.4
Release:        1
Summary:        GNOME Settings is GNOME's main interface for configuration of various aspects of your desktop.
License:        GPLv2+
URL:            http://www.gnome.org
Source0:        https://download.gnome.org/sources/gnome-control-center/44/%{name}-%{version}.tar.xz

Patch9001:      bugfix-fix_used_passwd_error_capture.patch
Patch9002:      bugfix-gnome-control-center-fix-repetitivewallpapers.patch
Patch9003:      gnome-control-center-change-translation-when-changing-password.patch
Patch9004:      gnome-control-center-remove-country-in-the-name-of-timezone.patch

BuildRequires:  chrpath gcc meson gettext cups-devel desktop-file-utils docbook-style-xsl libxslt
BuildRequires:  accountsservice-devel clutter-gtk-devel colord-devel 
BuildRequires:  gcr-devel gdk-pixbuf2-devel gtk3-devel gnome-online-accounts-devel
BuildRequires:  gsound-devel libgudev-devel ibus-devel libgtop2-devel 
BuildRequires:  pulseaudio-libs-devel libsecret-devel libxml2-devel ModemManager-glib-devel
BuildRequires:  pkgconfig(polkit-gobject-1) pkgconfig(pwquality) pkgconfig(smbclient)
BuildRequires:  pkgconfig(libwacom) pkgconfig(colord-gtk4) pkgconfig(libnma-gtk4)
BuildRequires:  pkgconfig(x11) pkgconfig(xi) pkgconfig(udisks2)
BuildRequires:  pkgconfig(gio-2.0) >= %{glib2_version}
BuildRequires:  pkgconfig(gnome-desktop-4) >= %{gnome_desktop_version}
BuildRequires:  pkgconfig(gnome-settings-daemon) >= %{gsd_version}
BuildRequires:  pkgconfig(goa-1.0) >= %{gnome_online_accounts_version}
BuildRequires:  pkgconfig(libadwaita-1) >= %{libadwaita_version}
BuildRequires:  pkgconfig(gsettings-desktop-schemas) >= %{gsettings_desktop_schemas_version}
BuildRequires:  pkgconfig(libnm) >= %{nm_version}
BuildRequires:  pkgconfig(gtk4) >= %{gtk4_version}
BuildRequires:  pkgconfig(upower-glib) >= %{upower_version}
BuildRequires:  pkgconfig(gnome-bluetooth-3.0) >= %{gnome_bluetooth_version}

Requires:       libadwaita%{?_isa} >= %{libadwaita_version}
Requires:       glib2%{?_isa} >= %{glib2_version}
Requires:       gnome-desktop3%{?_isa} >= %{gnome_desktop_version}
Requires:       gnome-online-accounts%{?_isa} >= %{gnome_online_accounts_version}
Requires:       gnome-settings-daemon%{?_isa} >= %{gsd_version}
Requires:       gsettings-desktop-schemas%{?_isa} >= %{gsettings_desktop_schemas_version}
Requires:       gtk4%{?_isa} >= %{gtk4_version}
Requires:       upower%{?_isa} >= %{upower_version}
Requires:       gnome-bluetooth%{?_isa} >= 1:%{gnome_bluetooth_version}
Requires:       %{name}-filesystem = %{version}-%{release}
Requires:       accountsservice alsa-lib colord cups-pk-helper dbus glx-utils iso-codes
Requires:       libgnomekbd

Recommends:     bolt
Recommends:     NetworkManager-wifi
Recommends:     nm-connection-editor
Recommends:     gnome-color-manager
Recommends:     gnome-remote-desktop
Recommends:     rygel
Recommends:     switcheroo-control
Recommends:     power-profiles-daemon

Provides:       control-center = 1:%{version}-%{release}
Provides:       control-center%{?_isa} = 1:%{version}-%{release}
Obsoletes:      control-center < 1:%{version}-%{release}

%description
Gnome-control-center is a graphical user interface to configure
various aspects of GNOME.
The configuration files that are picked up by the control-center
utilities are installed in directories contained by this package

%package filesystem
Summary:        Directories for %{name}
BuildArch:      noarch
Provides:       control-center-filesystem = 1:%{version}-%{release}
Obsoletes:      control-center-filesystem < 1:%{version}-%{release}

%description filesystem
The %{name}-filesystem contains directories where applications can
install configuration files that are picked up by the control-center
utilities in this package.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson \
  -Ddocumentation=true
%meson_build

%install
%meson_install
mkdir -p $RPM_BUILD_ROOT%{_datadir}/gnome/wm-properties

%find_lang %{name} --all-name --with-gnome
chrpath --delete $RPM_BUILD_ROOT%{_bindir}/gnome-control-center

%files -f %{name}.lang
%license COPYING
%{_bindir}/gnome-control-center
%{_datadir}/applications/*.desktop
%{_datadir}/bash-completion/completions/gnome-control-center
%{_datadir}/dbus-1/services/org.gnome.Settings.SearchProvider.service
%{_datadir}/dbus-1/services/org.gnome.Settings.service
%{_datadir}/gettext/
%{_datadir}/glib-2.0/schemas/org.gnome.Settings.gschema.xml
%{_datadir}/gnome-control-center/keybindings/*.xml
%{_datadir}/gnome-control-center/pixmaps
%{_datadir}/gnome-shell/search-providers/org.gnome.Settings.search-provider.ini
%{_datadir}/icons/gnome-logo-text*.svg
%{_datadir}/icons/hicolor/*/*/*
%{_metainfodir}/org.gnome.Settings.appdata.xml
%{_datadir}/pixmaps/faces
%{_datadir}/pkgconfig/gnome-keybindings.pc
%{_datadir}/polkit-1/actions/org.gnome.controlcenter.*.policy
%{_datadir}/polkit-1/rules.d/gnome-control-center.rules
%{_datadir}/sounds/gnome/default/*/*.ogg
%{_libexecdir}/cc-remote-login-helper
%{_libexecdir}/gnome-control-center-goa-helper
%{_libexecdir}/gnome-control-center-search-provider
%{_libexecdir}/gnome-control-center-print-renderer

%files filesystem
%dir %{_datadir}/gnome-control-center
%dir %{_datadir}/gnome-control-center/keybindings
%dir %{_datadir}/gnome/wm-properties

%files help
%doc NEWS README.md
%{_datadir}/man/man1/gnome-control-center.1*

%changelog
* Thu Nov 23 2023 lwg <liweiganga@uniontech.com> - 44.4-1
- update to version 44.4

* Tue Jan 10 2023 wenlong ding <wenlong.ding@turbolinux.com.cn> - 43.2-1
- Update to 43.2

* Tue Jun 21 2022 weijin deng <weijin.deng@turbolinux.com.cn> - 42.2-1
- Udpate to 42.2

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.0-1
- Udpate to 42.0

* Wed Aug 04 2021 chenyanpanHW <chenyanpan@huawei.com> - 3.38.6-2
- DESC: delete BuildRequires gdb

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.6-1
- Upgrade to 3.38.6
- Update Version, Release, BuildRequires, stage 'files'
- Delete 4 patches, modify the left 6 patches

* Wed Feb 3 2021 yanglu<yanglu60@huawei.com> - 3.30.1-13
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: Adapt to new gnome-desktop API changes

* Fri Sep 11 2020 chengguipeng<chengguipeng1@huawei.com> - 3.30.1-12
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify source0 url

* Mon Aug 10 2020 chengguipeng<chengguipeng1@huawei.com> - 3.30.1-11
- Delete BuildRequires libXxf86misc-devel for  that is legacy now.

* Fri Mar 20 2020 openEuler Buildteam<buildteam@openeuler.org> - 3.30.1-10
- add gdb in buildrequires

* Wed Mar 18 2020 openEuler Buildteam<buildteam@openeuler.org> - 3.30.1-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix crash when retrieving disk size

* Fri Mar 13 2020 openEuler Buildteam<buildteam@openeuler.org> - 3.30.1-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:remove country in the name of timezone

* Mon Dec 30 2019 daiqianwen<daiqianwen@huawei.com> - 3.30.1-7
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix old password can not check

* Sat Dec 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:optimization the spec

* Sat Sep 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:keep filesystem package

* Sat Aug 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add requires

* Thu Aug 22 2019 licunlong <licunlong@huawei.com> - 3.30.1-3.h2
- Type: NA
- ID: NA
- SUG: NA
- DESC: Remove patch prefix.

* Wed Jan 23 2019 gulining<gulining1@huawei.com> - 3.30.1-3.h1
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:gnome control center clickassist fix
	gnome control center fix repetitivewallpapers
	fix used passwd error capture
	gnome control center change translation when changing password
  
* Wed Oct 24 2018 openEuler Buildteam <buildteam@openeuler.org> - 3.30.1-2
- Package Init
